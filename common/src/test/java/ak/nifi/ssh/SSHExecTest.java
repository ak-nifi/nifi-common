package ak.nifi.ssh;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.jupiter.api.Test;

import java.util.List;

public class SSHExecTest {
    @Test
    public void test() {
        final TestRunner runner = TestRunners.newTestRunner(SSHExec.class);
        runner.setProperty(SSHExec.USER, "root");
        runner.setProperty(SSHExec.COMMAND, "ls /");
        runner.setProperty(SSHExec.SERVER_ADDRESS, "72.14.186.149");
        runner.setProperty(SSHExec.KEY_PATH, "/Users/akoira/.ssh/id_rsa");
        runner.run();
        runner.getFlowFilesForRelationship(SSHExec.REL_SUCCESS)
                .stream()
                .map(MockFlowFile::getContent)
                .forEach(System.out::println);
    }
}
