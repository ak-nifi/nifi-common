package ak.nifi.ssh;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.common.keyprovider.FileKeyPairProvider;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.apache.nifi.annotation.behavior.InputRequirement.Requirement.INPUT_ALLOWED;

@Tags({"ssh", "execute", "command"})
@CapabilityDescription("Execute remote ssh command")
@InputRequirement(INPUT_ALLOWED)
public class SSHExec extends AbstractProcessor {
    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("FlowFiles for which all content was collected.")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("In case a FlowFile generates an error during the ssh session, " +
                    "it will be routed to this relationship")
            .build();

    static final Relationship REL_ORIGINAL = new Relationship.Builder()
            .name("original")
            .description("the original file is sent to this relationship")
            .build();


    public static final PropertyDescriptor KEY_PATH = new PropertyDescriptor.Builder()
            .name("key.path")
            .displayName("ssh key path")
            .description("ssh *.pem key path")
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .defaultValue("/home/user/.aws/user.pem")
            .required(true)
            .build();

    public static final PropertyDescriptor SERVER_ADDRESS = new PropertyDescriptor.Builder()
            .name("server.address")
            .displayName("ssh server address")
            .description("ssh server ip or dnc name")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("localhost")
            .required(true)
            .build();

    public static final PropertyDescriptor SERVER_PORT = new PropertyDescriptor.Builder()
            .name("server.port")
            .displayName("ssh server port")
            .description("ssh server port")
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("22")
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .required(true)
            .build();

    public static final PropertyDescriptor USER = new PropertyDescriptor.Builder()
            .name("user")
            .displayName("ssh user")
            .description("ssh user")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .required(true)
            .build();

    public static final PropertyDescriptor COMMAND = new PropertyDescriptor.Builder()
            .name("command")
            .displayName("remote command")
            .description("remote command")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .required(true)
            .build();

    public static final PropertyDescriptor TIMEOUT = new PropertyDescriptor.Builder()
            .name("timeout")
            .displayName("ssh timeout")
            .description("ssh timeout")
            .addValidator(StandardValidators.TIME_PERIOD_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("10s")
            .required(true)
            .build();

    private static final List<PropertyDescriptor> properties = List.of(SERVER_ADDRESS, SERVER_PORT, USER, KEY_PATH, COMMAND, TIMEOUT);
    private static final Set<Relationship> relationships = Set.of(REL_SUCCESS, REL_FAILURE, REL_ORIGINAL);

    private String keyPath;
    private SshClient client;


    @Override
    protected final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public final Set<Relationship> getRelationships() {
        return relationships;
    }

    @OnScheduled
    public void onScheduled(ProcessContext context) {
        keyPath = context.getProperty(KEY_PATH).evaluateAttributeExpressions().getValue();
        client = SshClient.setUpDefaultClient();
        client.setKeyIdentityProvider(new FileKeyPairProvider(Path.of(keyPath)));
        client.start();
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile source = session.get();
        String address = context.getProperty(SERVER_ADDRESS).evaluateAttributeExpressions(source).getValue();
        Integer port = context.getProperty(SERVER_PORT).evaluateAttributeExpressions(source).asInteger();
        String user = context.getProperty(USER).evaluateAttributeExpressions(source).getValue();
        String command = context.getProperty(COMMAND).evaluateAttributeExpressions(source).getValue();
        Long timeOut = context.getProperty(TIMEOUT).evaluateAttributeExpressions(source).asTimePeriod(TimeUnit.MILLISECONDS);

        FlowFile file = null;
        if (source != null)
            file = session.create(source);
        else
            file = session.create();

        try (ClientSession sshSession = client.connect(user, address, port).verify(timeOut).getSession()) {
            if (!sshSession.auth().verify(timeOut).isSuccess())
                onError(file, session, "cannot auth: " + user + "@" + address, null);
            String out = sshSession.executeRemoteCommand(command);
            session.write(file, o -> o.write(out.getBytes(StandardCharsets.UTF_8)));
            session.transfer(file, REL_SUCCESS);
        } catch (Exception e) {
            onError(file, session, e.getMessage(), e);
        } finally {
            if (source != null)
                session.transfer(source, REL_ORIGINAL);
        }
    }

    public final void onError(FlowFile file, ProcessSession session, String message, Throwable throwable) {
        session.putAttribute(file, "error.message", message);
        if (throwable != null)
            session.putAttribute(file, "error.stack.trace", ExceptionUtils.getStackTrace(throwable));
        session.transfer(session.penalize(file), REL_FAILURE);
    }
}
