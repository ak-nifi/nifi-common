# nifi-crawler

nifi-crawler


# resources 
* https://www.avvo.com/
* https://www.lawyer.com/


* docker 

docker run -d -p 4444:4444 -p 7900:7900 --shm-size="2g" -e JAVA_OPTS=-Xmx512m selenium/standalone-chrome-debug:latest

# Update dependencies

*The project uses **Gradle Versions Plugin***

1. **./gradlew dependencyUpdates** - show report about all dependencies in project
2. Change plugin versions in a file **versions.gradle** and reload gradle

For update **GRADLE** itself:

1. Find *gradleVersion* prop in a file **build.gradle** and change version
2. Reload Gradle
3. Execute command - ./gradlew wrapper

Execute command - ./gradlew publishToMavenLocal